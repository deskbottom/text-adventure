class Player:
  def __init__(self, area):
    self.money = 100
    self.area = area
    self.location = None
    self.items = []

  # INVENTORY INTERACTION #
  def add_item(self, item):
    """ Adds given item to inventory. """
    self.items.append(item)
  
  def remove_item(self, title):
    """ Removes item from inventory based on given title. """
    self.items.remove(self.get_item(title))

  def get_item(self, title):
    """ Returns item matching given title. """
    for item in self.items:
      if item.title == title:
        return item

  # COMMANDS #
  def drop(self, target):
    """ Takes item from inventory and adds it to current location. """
    item = self.get_item(target)
    if item != None:
      self.remove_item(target)
      self.location.add_item(item)
  
  def take(self, target):
    """ Adds target item to inventory and removes it from current location. """
    item = self.location.get_item(target)
    if item != None:
      self.items.append(item)
      self.location.remove_item(target)
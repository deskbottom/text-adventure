from tkinter import *

class Window(Frame):
  def __init__(self):
    self.root = Tk()
    self.root.geometry('900x600')
    Frame.__init__(self, self.root, bg='black')
    self.init_window()
    self.recently_chosen = None

  def init_window(self):
    self.root.title('GUI')
    self.pack(fill=BOTH, expand=1)
  
  def fill_window(self, title, options):
    self.clear_window()
    label = Label(self,
                  text = title,
                  font = ('Arial', 12),
                  bg = 'black',
                  foreground = 'white')
    label.pack(pady=8)

    for option in options:
      button = Button(self,
                      text = option,
                      font = ('Arial', 10),
                      bg = 'black',
                      activebackground = 'gray',
                      foreground = 'white',
                      activeforeground = 'white',
                      command = lambda option=option: self.choose(option))
      button.pack(pady=8)
    
    self.mainloop()
  
  def clear_window(self):
    for widget in self.winfo_children():
      widget.destroy()

  def choose(self, option):
    self.recently_chosen = option
    self.root.quit()
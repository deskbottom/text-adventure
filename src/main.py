from player.player import Player
from data.data import Data
from window import Window
import os

class Game:
  def __init__(self):
    self.data = Data()
    os.system('clear')
    self.window = Window()
    self.introduction()

  def introduction(self):
    """ Initializes player and prints introductory information. """
    self.prompt('Intro text', ['Continue'])
    self.player = Player(self.data.get_area(0))
    self.view_area()

  def prompt(self, title, options):
    """ Fills window with title text and options, and returns user's selection. """
    self.window.fill_window(title, options)
    return self.window.recently_chosen

  # Views
  def view_area(self):
    """ Displays current area details and prompts user for action. """
    self.player.location = None
    options = ['Enter Location', 'Inventory', 'Move']
    choice = self.prompt(self.player.area.details(), options)
    if choice == 'Enter Location':
      self.view_locations()
    elif choice == 'Inventory':
      self.view_items(True)
    elif choice == 'Move':
      self.view_exits()

  def view_exits(self):
    """ Displays current location's exits and calls player's move method. """
    exits = list(self.player.area.exits.keys())
    direction = self.prompt('Move where?', exits + ['Back'])
    if direction != 'Back':
      area_id = self.player.area.exits[direction]
      self.player.area = self.data.get_area(area_id)
    self.view_area()

  def view_locations(self):
    """ Displays locations in current area and moves player to selected location. """
    location_titles = [location.title for location in self.player.area.locations]
    location_string = self.prompt('Where to?', location_titles + ['Back'])
    if location_string == 'Back':
      self.view_area()
    else:
      location_index = location_titles.index(location_string)
      self.player.location = self.player.area.locations[location_index]
      self.view_location()

  def view_location(self):
    """ Displays current location details and prompts user for an action. """
    options = ['View Items', 'Inventory', 'Back']
    choice = self.prompt(self.player.location.details(), options)

    if choice == 'View Items':
      self.view_items(False)
    elif choice == 'Inventory':
      self.view_items(True)
    elif choice == 'Back':
      self.view_locations()

  def view_items(self, player_container, error_message=''):
    """ Determines whether container is player or location, then displays items. """
    title = error_message
    if player_container:
      title += 'Items you are carrying'
      container = self.player
    else:
      title += 'Items in this location'
      container = self.player.location
    
    item = self.prompt(title, [item.title for item in container.items] + ['Back'])
    if item == 'Back':
      if self.player.location == None:
        self.view_area()
      else:
        self.view_location()
    else:
      self.view_item(player_container, item)
  
  def view_item(self, player_container, item):
    """ Displays item details and prompts user for action. """
    error_message = ''
    if player_container:
      options = ['Drop']
      container = self.player
    else:
      options = ['Take']
      container = self.player.location
    
    title = container.get_item(item).title
    if container.get_item(item).text != None:
      title += '\n\nText\n' + container.get_item(item).text
    action = self.prompt(title, options + ['Back'])
    if action == 'Drop':
      if self.player.location == None:
        error_message = 'NOTE: You must be in a location in order to drop an item.\n'
      else:
        self.player.drop(item)
    elif action == 'Take':
      self.player.take(item)
    self.view_items(player_container, error_message)

game = Game()
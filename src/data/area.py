class Area:
  def __init__(self, data, attributes):
    self.data = data
    self.id = attributes['id']
    self.title = attributes['title']
    self.description = attributes['desc']
    self.exits = attributes['exits']
    self.locations = []
    for location in attributes['locations']:
      self.locations.append(self.data.get_location(location))
    
  def details(self):
    """ Returns string with details including description and locations. """
    result = self.description + '\n'
    for direction in self.exits:
      exit_title = self.data.get_area(self.exits[direction]).title
      result += 'To the ' + direction + ' is a ' + exit_title + '.\n'
    return result
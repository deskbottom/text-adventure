import json

from data.area import Area
from data.location import Location

class Data:
  def __init__(self):
    self.locations = self.create_locations()
    self.areas = self.create_areas()
  
  def create_areas(self):
    with open('data/areas.json', 'r') as f:
      areas = json.load(f)
    return [Area(self, area) for area in areas]
  
  def create_locations(self):
    with open('data/locations.json', 'r') as f:
      locations = json.load(f)
    return [Location(location) for location in locations]

  def get_area(self, id):
    for area in self.areas:
      if area.id == id:
        return area

  def get_location(self, id):
    for location in self.locations:
      if location.id == id:
        return location
from item import Item

class Location:
  def __init__(self, attributes):
    self.id = attributes['id']
    self.title = attributes['title']
    self.description = attributes['desc']
    self.items = []
    if 'items' in attributes:
      for item in attributes['items']:
        self.items.append(Item(item))
  
  def details(self):
    """ Returns string with details including description and exits. """
    result = self.description + '\n'
    return result
  
  # Adds given item to items list.
  def add_item(self, item):
    """ Adds given item to items list. """
    self.items.append(item)

  def get_item(self, title):
    """ Returns item matching given title. """
    for item in self.items:
      if item.title == title:
        return item

  def remove_item(self, title):
    """ Removes item matching given title. """
    self.items.remove(self.get_item(title))
class Item:
  def __init__(self, attributes):
    self.title = attributes['title']
    self.text = None
    if 'text' in attributes:
      self.text = attributes['text']
#!/bin/bash
path="src/main.py"
if [[ `python3 --version` == "Python 3"* ]]; then
  python3 $path
elif [[ `python --version` == "Python 3"* ]]; then
  python $path
else
  echo "Python must be installed to run this application."
fi
# [insert text-game title here]

This project is an open world text-based RPG with an emphasis on realism and immersion. I am writing code with a heavy focus on the underlying engine, for possible future uses. The code relies on JSON files to store all data. I plan to develop this project to have a Python engine with almost all game story, items, and locations stored in JSON files. If anybody would like to contribute, please send me an e-mail!

## Usage
From the root of the project directory:

    $ bash start.sh

As of right now, the game can only run from a Bash environment that has Python 3. If you would really like to play (for some reason), follow the above instructions using Git Bash (available on both Windows and Mac OS).
from random import randint
import random

from body import Body

class Animal:
  def __init__(self, name, strength, toughness, speed, arms, legs):
    self.name = name

    # stats
    self.hydration  = 100
    self.fullness   = 100
    self.strength   = strength    # max: 10
    self.speed      = speed       # max: 100
    self.toughnesss = toughness   # max: 100
    self.body       = Body(arms, legs)
  
  def alive(self):
    """ Determines whether animal is alive or not. """
    return self.consciousness() > 0

  # Calculates consciousness based on hydration, fullness, and head health.
  def consciousness(self):
    """ Calculates consciousnesses based on health of head. """
    return self.body.head / 100 # temporary

  def average_arm_health(self):
    """ Calculates average health of all arms. """
    total = 0
    for arm in self.body.arms:
      total = total + self.body.arms[arm]
    return (total / float(len(self.body.arms))) / 100

  def average_leg_health(self):
    """ Calculates average health of all legs. """
    total = 0
    for leg in self.body.legs:
      total = total + self.body.legs[leg]
    return (total / float(len(self.body.legs))) / 100

  # Calculates strength based on average health of arms.
  def calculated_strength(self):
    """ Calculates strength based on base strength, arm health, and consciousness. """
    return self.strength * self.average_arm_health() * self.consciousness()

  def calculated_attack_speed(self):
    """ Calculates attack speed based on base speed, arm health, abdomen health, and consciousness. """
    abdomen_status = self.body.abdomen / 100
    return self.speed * self.average_arm_health() * abdomen_status * self.consciousness()

  def calculated_dodge_speed(self):
    """ Calculates dodge speed based on base speed, leg health, abdomen health, and consciousness. """
    abdomen_status = self.body.abdomen / 100
    return self.speed * self.average_leg_health() * abdomen_status * self.consciousness()

  def take_hit(self, attacker, part, damage):
    """ Based on hit chance, either dodges or takes damage for an incoming hit. """
    hit_chance = (attacker.calculated_attack_speed() - self.calculated_dodge_speed())/2 + 50
    result_string = attacker.name + ' is attacking ' + self.name + '\'s ' + part + ' (' + str(hit_chance) + '%)'

    if randint(0, 100) < hit_chance:
      self.body.health(part, damage * -1)
      print(result_string + ' - hit for ' + str(damage))
    else:
      print(result_string + ' - miss')

  def attack(self, target, part):
    """ Attempt to hit given enemy at given part. """
    target.take_hit(self, part, self.calculated_strength())
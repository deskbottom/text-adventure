import random

from animal import Animal

class Combat:
  def __init__(self, player, enemy):
    self.player = player
    self.enemy = enemy
    self.longer = self.determine_length()['longer']
    self.shorter = self.determine_length()['shorter']

    self.fight()
    self.winner = self.determine_winner()['winner']
    self.loser = self.determine_winner()['loser']
    print(self.winner.name + ' defeats ' + self.loser.name)

  def fight(self):
    """ Take combat turn until player or enemy has died. """
    first = self.determine_order()['first']
    second = self.determine_order()['second']
    while first.alive() and second.alive():
      self.take_turn(first, second)

  def take_turn(self, first, second):
    """ Prints health of participants, and has them attack each other. """
    print(chr(27) + "[2J")
    self.print_health()
    print('\n')
    self.choose_attack() if first == self.player else first.attack(second, random.choice(second.body.parts()))
    if second.alive():
      self.choose_attack() if second == self.player else second.attack(first, random.choice(first.body.parts()))
    raw_input('Press any key to continue.')

  def print_health(self):
    """ Prints a formatted paragraph describing health of the player and enemy. """
    longer_parts = self.longer.body.parts()
    shorter_parts = self.shorter.body.parts()
    print('{:30} {:30}'.format(self.shorter.name, self.longer.name))
    i = 0
    while i < len(shorter_parts):
      print('{:30} {:30}'.format
          (shorter_parts[i] + ': ' + str(self.shorter.body.health(shorter_parts[i])), 
            longer_parts[i] + ': ' + str(self.longer.body.health(longer_parts[i]))))
      i += 1
    while i < len(longer_parts):
      print('{:30} {:30}'.format('', longer_parts[i] + ': ' + str(self.longer.body.health(longer_parts[i]))))
      i += 1

  def choose_attack(self):
    """ Prompts user for body part to attack, verifies it exists, and executes the command. """
    part = raw_input('Select part to attack: ').strip()
    if enemy.body.health(part):
      self.player.attack(self.enemy, part)
    else:
      self.choose_attack()

  def determine_order(self):
    """ Determines attack order. """
    return {
      'first': self.player,
      'second': self.enemy
    }

  def determine_length(self):
    """ Determines whether the player or enemy has a longer body part count. """
    longer = self.player
    shorter = self.enemy
    if len(self.enemy.body.parts()) > len(self.player.body.parts()):
      longer = self.enemy
      shorter = self.player
    
    return {
      'longer': longer,
      'shorter': shorter
    }
  
  def determine_winner(self):
    """ Based on whether player is living or not, determines combat winner and loser. """
    if self.player.alive():
      return {
        'winner': self.player,
        'loser': self.enemy
      }
    else:
      return {
        'winner': self.enemy,
        'loser': self.player
      }

player = Animal('Player', 10, 100, 100, ['left', 'right'], ['left', 'right'])
enemy = Animal('Enemy', 10, 100, 100, ['left', 'right'], ['left', 'middle','right'])
c = Combat(player, enemy)
class Body:
  def __init__(self, arms, legs):
    self.head = 100
    self.chest = 100
    self.abdomen = 100
    self.arms = self.initialize_arms(arms)
    self.legs = self.initialize_legs(legs)
  
  def initialize_arms(self, arms):
    result = {}
    for arm in arms:
      result[arm] = 100
    return result
  
  def initialize_legs(self, legs):
    result = {}
    for leg in legs:
      result[leg] = 100
    return result

  def print_health(self):
    print('Head: ' + str(self.head))
    print('Chest: ' + str(self.chest))
    print('Abdomen: ' + str(self.abdomen))
    for arm in self.arms:
      print(arm.title() + ' Arm: ' + str(self.arms[arm]))
    for leg in self.legs:
      print(leg.title() + ' Leg: ' + str(self.legs[leg]))
  
  def parts(self):
    result = ['head', 'chest', 'abdomen']
    for arm in self.arms:
      result.append(arm + ' arm')
    for leg in self.legs:
      result.append(leg + ' leg')
    return result
  
  # Finds a given body part ('left arm' or 'head') and takes health from it. Returns remaining health of the body part.
  def health(self, search, value=0):
    search = search.strip()
    if search.lower() == 'head':
      self.head = self.head + value
      return self.head
    elif search.lower() == 'chest':
      self.chest = self.chest + value
      return self.chest
    elif search.lower() == 'abdomen':
      self.abdomen = self.abdomen + value
      return self.abdomen
    # multi-word searches such as 'left arm'
    elif search.count(' ') > 0:
      if search.lower().split()[1] == 'arm':
        side = search.lower().split()[0]
        if side in self.arms:
          self.arms[side] = self.arms[side] + value
          return self.arms[side]
      elif search.lower().split()[1] == 'leg':
        side = search.lower().split()[0]
        if side in self.legs:
          self.legs[side] = self.legs[side] + value
          return self.legs[side]
    
    print(search + ' is not a valid body part.')